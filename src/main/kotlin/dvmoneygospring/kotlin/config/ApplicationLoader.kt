package dvmoneygospring.kotlin.config

import dvmoneygospring.kotlin.entity.Transaction
import dvmoneygospring.kotlin.entity.TransactionType
import dvmoneygospring.kotlin.entity.User
import dvmoneygospring.kotlin.entity.Wallet
import dvmoneygospring.kotlin.repository.TransactionRepository
import dvmoneygospring.kotlin.repository.UserRepository
import dvmoneygospring.kotlin.repository.WalletRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
class ApplicationLoader: ApplicationRunner{
    @Autowired
    lateinit var userRepository: UserRepository
    @Autowired
    lateinit var walletRepository: WalletRepository
    @Autowired
    lateinit var transactionRepository: TransactionRepository
    @Transactional

    override fun run(args: ApplicationArguments?){

        var user1 = userRepository.save(User("Janey", "Chanapha","Janey@gmail.com","1111"))
        var user2 = userRepository.save(User("Ben", "Jamin","Ben@gmail.com","2222"))
        var user3 = userRepository.save(User("Jame", "Collin","Jame@gmail.com","3333"))

        var wallet1 = walletRepository.save(Wallet("Poor", 500.0))
        var wallet2 = walletRepository.save(Wallet("Money", 1000.0))
        var wallet3 = walletRepository.save(Wallet("Rich", 1500.0))

        var transaction1 = transactionRepository.save(Transaction(500.0,
                "Salary",
                "12/4/2019",
                TransactionType.INCOME))

        var transaction2 = transactionRepository.save(Transaction(1000.0,
                "Freelance",
                "5/7/2019",
                TransactionType.INCOME))

        var transaction3 = transactionRepository.save(Transaction(500.0,
                "Utilities",
                "10/1/2019",
                TransactionType.EXPENSE))

        var transaction4 = transactionRepository.save(Transaction(300.0,
                "Foods",
                "20/2/1019",
                TransactionType.EXPENSE))

        user1.wallets.add(wallet1)
        wallet1.transactions.add(transaction1)

        user2.wallets.add(wallet2)
        wallet2.transactions.add(transaction2)

        user3.wallets.add(wallet3)
        wallet3.transactions.add(transaction3)
        wallet3.transactions.add(transaction4)


    }
}