package dvmoneygospring.kotlin.Util

import dvmoneygospring.kotlin.entity.Transaction
import dvmoneygospring.kotlin.entity.User
import dvmoneygospring.kotlin.entity.Wallet
import dvmoneygospring.kotlin.entity.dto.TransactionDto
import dvmoneygospring.kotlin.entity.dto.UserDto
import dvmoneygospring.kotlin.entity.dto.WalletDto
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "spring")
interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    fun mapUserDto(user: User?): UserDto?
    fun mapUserDto(user: List<User>): List<UserDto>
    @InheritInverseConfiguration
    fun mapUserDto(userDto: UserDto): User

    fun mapWalletDto(wallet: Wallet?): WalletDto?
    fun mapWalletDto(wallet: List<Wallet>): List<WalletDto>
    @InheritInverseConfiguration
    fun mapWalletDto(walletDto: WalletDto): Wallet

    fun mapTransactionDto(transaction: Transaction?): TransactionDto?
    fun mapTransactionDto(transaction: List<Transaction>): List<TransactionDto>
    @InheritInverseConfiguration
    fun mapTransactionDto(transactionDto: TransactionDto): Transaction


}