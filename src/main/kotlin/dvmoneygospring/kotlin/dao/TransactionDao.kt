package dvmoneygospring.kotlin.dao;

import dvmoneygospring.kotlin.entity.Transaction

interface TransactionDao {
    fun save(transaction: Transaction): Transaction
    fun getAllTransactions(): List<Transaction>
}