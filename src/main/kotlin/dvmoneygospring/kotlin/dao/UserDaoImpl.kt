package dvmoneygospring.kotlin.dao

import dvmoneygospring.kotlin.entity.User
import dvmoneygospring.kotlin.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class UserDaoImpl: UserDao {
    override fun getUserByName(name: String): User? {
        return userRepository.findAllByEmail(name)
    }

    override fun getUserById(id: Long): User {
        return userRepository.findById(id).orElse(null)
    }

    override fun save(user: User): User {
        return userRepository.save(user)
    }

    override fun getUsers(): List<User> {
        return userRepository.findAll().filterIsInstance(User::class.java)
    }

    @Autowired
    lateinit var userRepository: UserRepository

}