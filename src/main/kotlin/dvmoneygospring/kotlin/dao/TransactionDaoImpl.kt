package dvmoneygospring.kotlin.dao

import dvmoneygospring.kotlin.entity.Transaction
import dvmoneygospring.kotlin.repository.TransactionRepository
import dvmoneygospring.kotlin.repository.WalletRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class TransactionDaoImpl: TransactionDao {

    @Autowired
    lateinit var transactionRepository: TransactionRepository

    override fun getAllTransactions(): List<Transaction> {
        return transactionRepository.findAll().filterIsInstance(Transaction::class.java)
    }

    override fun save(transaction: Transaction): Transaction {
        return transactionRepository.save(transaction)
    }

}