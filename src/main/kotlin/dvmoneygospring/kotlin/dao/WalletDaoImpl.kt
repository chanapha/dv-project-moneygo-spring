package dvmoneygospring.kotlin.dao

import dvmoneygospring.kotlin.entity.User
import dvmoneygospring.kotlin.entity.Wallet
import dvmoneygospring.kotlin.repository.WalletRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class WalletDaoImpl : WalletDao {

    override fun getWalletById(id: Long): Wallet {
        return walletRepository.findById(id).orElse(null)
    }

    override fun save(wallet: Wallet): Wallet {
        return walletRepository.save(wallet)
    }

    override fun getWallets(): List<Wallet> {
        return walletRepository.findAll().filterIsInstance(Wallet::class.java)
    }

    @Autowired
    lateinit var walletRepository: WalletRepository


}