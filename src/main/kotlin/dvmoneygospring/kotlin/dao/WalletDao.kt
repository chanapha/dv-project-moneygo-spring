package dvmoneygospring.kotlin.dao

import dvmoneygospring.kotlin.entity.Wallet

interface WalletDao {
     fun getWallets(): List<Wallet>
     fun getWalletById(id: Long): Wallet
     fun save(wallet: Wallet): Wallet
}