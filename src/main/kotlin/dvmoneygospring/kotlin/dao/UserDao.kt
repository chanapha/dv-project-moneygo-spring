package dvmoneygospring.kotlin.dao

import dvmoneygospring.kotlin.entity.User

interface UserDao{

    fun getUsers(): List<User>

    fun getUserById(id: Long): User

    fun save(user: User): User

     fun getUserByName(name: String): User?

}