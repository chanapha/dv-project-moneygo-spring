package dvmoneygospring.kotlin.Service

import dvmoneygospring.kotlin.entity.Transaction

interface TransactionService{

     fun getTransactions(): List<Transaction>
     fun getTransactionsByWalletId(walletId: Long): List<Transaction>
     fun getTransactionsByWalletIdByDateDesc(walletId: Long): List<Transaction>
     fun addTransactionTo(walletId: Long, transaction: Transaction): Transaction
}