package dvmoneygospring.kotlin.Service

import dvmoneygospring.kotlin.dao.UserDao
import dvmoneygospring.kotlin.dao.WalletDao
import dvmoneygospring.kotlin.entity.Wallet
import dvmoneygospring.kotlin.entity.dto.WalletDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class WalletServicelmpl : WalletService {

    @Autowired
    lateinit var walletDao: WalletDao

    @Autowired
    lateinit var userDao: UserDao

    @Transactional
    override fun addWalletToUser(userId: Long, wallet: Wallet): Wallet {
        val user = userDao.getUserById(userId)
        val newWallet = walletDao.save(wallet)
        user.wallets.add(newWallet)
        return newWallet
    }

    @Transactional
    override fun getWalletsByUserId(userId: Long): List<Wallet> {
//        val results = mutableListOf<Wallet>()
//        val user = userDao.getUserById(userId)
//        for (wallet in user.wallets) {
//            results.add(wallet)
//        }
//        return results
        return userDao.getUserById(userId).wallets
    }

    override fun save(wallet: Wallet): Wallet {
        return walletDao.save(wallet)
    }

    override fun getWallets(): List<Wallet> {
        return walletDao.getWallets()
    }

}