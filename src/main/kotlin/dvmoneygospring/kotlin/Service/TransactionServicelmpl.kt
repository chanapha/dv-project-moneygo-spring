package dvmoneygospring.kotlin.Service

import dvmoneygospring.kotlin.dao.TransactionDao
import dvmoneygospring.kotlin.dao.WalletDao
import dvmoneygospring.kotlin.entity.Transaction
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class TransactionServicelmpl : TransactionService {

    @Transactional
    override fun getTransactionsByWalletIdByDateDesc(walletId: Long): List<Transaction> {
        val transactions = this.getTransactionsByWalletId(walletId)
        return transactions.sortedByDescending { it.timestamp }
    }

    @Autowired
    lateinit var transactionDao: TransactionDao

    @Autowired
    lateinit var walletDao: WalletDao

    @Transactional
    override fun addTransactionTo(walletId: Long, transaction: Transaction): Transaction {
        val wallet = walletDao.getWalletById(walletId)
        val newTransaction = transactionDao.save(transaction)
        wallet.transactions.add(newTransaction)
        return newTransaction
    }

    override fun getTransactions(): List<Transaction> {
        return transactionDao.getAllTransactions()
    }

    override fun getTransactionsByWalletId(walletId: Long): List<Transaction> {
        val currentWallet = walletDao.getWalletById(walletId)
        return currentWallet.transactions
    }
}
