package dvmoneygospring.kotlin.Service

import dvmoneygospring.kotlin.entity.Wallet
import dvmoneygospring.kotlin.entity.dto.WalletDto

interface WalletService{
     fun getWallets(): List<Wallet>
     fun getWalletsByUserId(userId: Long): List<Wallet>
     fun save(wallet: Wallet): Wallet
     fun addWalletToUser(userId: Long, wallet: Wallet): Wallet
}