package dvmoneygospring.kotlin.Service

import dvmoneygospring.kotlin.dao.UserDao
import dvmoneygospring.kotlin.entity.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class UserServicelmpl: UserService{
    override fun getUserByName(name: String): User?{
        return userDao.getUserByName(name)
    }



    @Transactional
    override fun save(user: User): User {
        return userDao.save(user)
    }

    @Autowired
    lateinit var userDao: UserDao


    override fun getUsers(): List<User> {
        return userDao.getUsers()
    }


}