package dvmoneygospring.kotlin.Service

import dvmoneygospring.kotlin.entity.User

interface UserService{
     fun getUsers(): List<User>
     fun save(user: User): User
     fun getUserByName(name: String): User?

}