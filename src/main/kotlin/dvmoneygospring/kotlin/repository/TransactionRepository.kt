package dvmoneygospring.kotlin.repository

import dvmoneygospring.kotlin.entity.Transaction
import org.springframework.data.repository.CrudRepository

interface TransactionRepository: CrudRepository<Transaction, Long>