package dvmoneygospring.kotlin.repository

import dvmoneygospring.kotlin.entity.Wallet
import org.springframework.data.repository.CrudRepository

interface WalletRepository: CrudRepository<Wallet, Long>