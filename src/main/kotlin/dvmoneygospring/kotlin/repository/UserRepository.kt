package dvmoneygospring.kotlin.repository

import dvmoneygospring.kotlin.entity.User
import org.springframework.data.repository.CrudRepository

interface UserRepository: CrudRepository<User, Long> {
     fun findAllByEmail(name: String): User?
}