package dvmoneygospring.kotlin.entity

import java.time.LocalDate
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Transaction(
        open var value: Double? = null,
        open var category: String? = null,
        open var timestamp: String? = null,
        open var type: TransactionType? = null
) {
    @Id
    @GeneratedValue
    var id: Long? = null
}