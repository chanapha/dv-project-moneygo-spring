package dvmoneygospring.kotlin.entity.dto

import dvmoneygospring.kotlin.entity.TransactionType

class TransactionDto(
        var value: Double? = null,
        var category: String? = null,
        var type: TransactionType? = null,
        var timestamp: String? = null
)