package dvmoneygospring.kotlin.entity.dto

class UserDto( var id: Long? = null,
               var firstName: String? = null,
               var lastName: String? = null,
               var email: String? = null,
               var password: String? = null

)