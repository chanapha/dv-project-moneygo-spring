package dvmoneygospring.kotlin.entity.dto

class WalletDto(
        var id: Long? =null,
        var walletName: String? = null,
        var balance: Double? = null
)