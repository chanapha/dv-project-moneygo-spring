package dvmoneygospring.kotlin.entity

import javax.persistence.*

@Entity
data class Wallet(
        var walletName: String? = null,
        var balance: Double? = null
) {

    @Id
    @GeneratedValue
    var id: Long? = null

    @OneToMany
    var transactions = mutableListOf<Transaction>()
}