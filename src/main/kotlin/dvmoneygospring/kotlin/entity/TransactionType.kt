package dvmoneygospring.kotlin.entity

enum class TransactionType {
   INCOME, EXPENSE
}