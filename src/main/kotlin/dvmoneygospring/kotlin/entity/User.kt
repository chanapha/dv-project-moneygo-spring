package dvmoneygospring.kotlin.entity

import javax.persistence.*

@Entity
data class User(
        var firstName: String? = null,
        var lastName: String? = null,
        var email: String? = null,
        var password: String? = null

) {
    @Id
    @GeneratedValue
    var id: Long? = null

//    @ManyToOne
//    lateinit var wallet : Wallet

    @OneToMany
    var wallets = mutableListOf<Wallet>()
}