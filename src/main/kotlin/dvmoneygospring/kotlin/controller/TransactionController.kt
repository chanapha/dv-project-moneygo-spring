package dvmoneygospring.kotlin.controller

import dvmoneygospring.kotlin.Service.TransactionService
import dvmoneygospring.kotlin.Util.MapperUtil
import dvmoneygospring.kotlin.dao.TransactionDao
import dvmoneygospring.kotlin.entity.dto.TransactionDto
import org.hibernate.Transaction
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class TransactionController {


    @Autowired
    lateinit var transactionService: TransactionService

    @GetMapping("/transaction")
    fun getAllTransactions(): ResponseEntity<Any> {
        return ResponseEntity.ok(transactionService.getTransactions())
    }

    @GetMapping("transaction/walletID/{walletId}")
    fun getTransactionsByWalletId(@PathVariable walletId: Long): ResponseEntity<Any> {
        return ResponseEntity.ok(transactionService.getTransactionsByWalletId(walletId))
    }

    @GetMapping("transaction/walletIDByDate/{walletId}")
    fun getTransactionsByWalletIdByDateDesc(@PathVariable walletId: Long): ResponseEntity<Any> {
        return ResponseEntity.ok(transactionService.getTransactionsByWalletIdByDateDesc(walletId))
    }

    @PostMapping("/transaction/addTransactionTo/{walletId}")
    fun addTransactionToWallet(@PathVariable walletId: Long, @RequestBody transaction: TransactionDto): ResponseEntity<Any> {
        val output = transactionService.addTransactionTo(walletId, MapperUtil.INSTANCE.mapTransactionDto(transaction))
        val outputDto = MapperUtil.INSTANCE.mapTransactionDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build()
    }

}