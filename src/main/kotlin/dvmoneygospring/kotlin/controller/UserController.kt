package dvmoneygospring.kotlin.controller

import dvmoneygospring.kotlin.Service.UserService
import dvmoneygospring.kotlin.Service.WalletService
import dvmoneygospring.kotlin.Util.MapperUtil
import dvmoneygospring.kotlin.entity.dto.UserDto
import dvmoneygospring.kotlin.entity.dto.WalletDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class UserController {

    @Autowired
    lateinit var userService: UserService
    @Autowired
    lateinit var walletService: WalletService

    @GetMapping("/user")
    fun getAllUser(): ResponseEntity<Any> {
        return ResponseEntity.ok(userService.getUsers());
    }

    @PostMapping("user/addUser")
    fun addTransaction(
            @RequestBody user: UserDto): ResponseEntity<Any> {
        val output = userService.save(MapperUtil.INSTANCE.mapUserDto(user))
        val outputDto = MapperUtil.INSTANCE.mapUserDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build()
    }


    @GetMapping("/user/query")
    fun getUserByEmail(@RequestParam("email") name: String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapUserDto(userService.getUserByName(name))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }



}