package dvmoneygospring.kotlin.controller

import dvmoneygospring.kotlin.Service.TransactionService
import dvmoneygospring.kotlin.Service.WalletService
import dvmoneygospring.kotlin.Util.MapperUtil
import dvmoneygospring.kotlin.entity.dto.WalletDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class WalletController {

    @Autowired
    lateinit var walletService: WalletService

    @GetMapping("/wallet")
    fun getAllWallet(): ResponseEntity<Any> {
        return ResponseEntity.ok(walletService.getWallets())
    }

    @GetMapping("/wallet/userID/{userId}")
    fun getWalletsByUserId(
            @PathVariable userId: Long): ResponseEntity<Any> {
        val output = walletService.getWalletsByUserId(userId)
        return ResponseEntity.ok(output)
    }

    @PostMapping("/wallet/addWalletTo/{userId}")
    fun addWalletToUser(
            @PathVariable userId: Long,
            @RequestBody wallet: WalletDto
    ) : ResponseEntity<Any>{
        val output = walletService.addWalletToUser(userId, MapperUtil.INSTANCE.mapWalletDto(wallet))
        val outputDto = MapperUtil.INSTANCE.mapWalletDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build()
    }

}